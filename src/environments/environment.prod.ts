export const environment = {
  production: true,
  apiBaseUrl: 'http://cashbook.lan',
  ckEditorConfig: {
    toolbar: {
      items: ['heading', 'bold', 'italic', 'link', 'bulletedList', 'numberedList'
      ]
    }
  },
  senderEmail: 'mail@bitkorn.de',
  senderName: 'Dev Bitkorn'
};
