export const client = {uuid: '1bc7728f-61fc-4b76-b575-79931bd3c046', label: 'Bitkorn freelancer'};
export const dateFormatDe = 'dd.MM.yyyy';
export const dateFormatTimestamp = 'yyyy-MM-dd';
export const datePickerConf = {format: 'YYYY-MM-DD'};
