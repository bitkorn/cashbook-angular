import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {UserService} from './cashbook/shared/service/user.service';
import {NavigationStart, Router, RouterEvent} from '@angular/router';
import {CashbookService} from './cashbook/shared/service/cashbook.service';
import {filter} from 'rxjs/operators';
import * as $ from 'jquery';
import * as appPrefs from './app-prefs';
import {L10N_CONFIG, L10N_LOCALE, L10nConfig, L10nLocale, L10nTranslationService} from 'angular-l10n';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  subscriptions = new Set<Subscription>();
  title = 'cashbookAngular';
  isLoading = false;
  client = {uuid: '', label: ''};

  schema = this.l10nConfig.schema;

  constructor(
    private router: Router,
    public userService: UserService,
    private cashbookService: CashbookService,
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    @Inject(L10N_CONFIG) private l10nConfig: L10nConfig,
    private translation: L10nTranslationService
  ) {
    this.client = appPrefs.client;
    this.subscriptions.add(this.cashbookService.isLoading.subscribe((event: boolean) => {
      this.isLoading = event;
    }));
  }

  ngOnInit(): void {
    this.userService.sessionCheck();
    this.router.events.pipe(
      filter((event) => event instanceof NavigationStart)
    ).subscribe((event: NavigationStart) => {
      if (event.url !== '/login') {
      }
    });
    this.subscriptions.add(this.translation.onChange().subscribe({
      next: (locale: L10nLocale) => {
        console.log(locale);
        console.log(this.translation.data);
      }
    }));
    this.subscriptions.add(this.translation.onError().subscribe({
      next: (error: any) => {
        if (error) {
          console.log(error);
        }
      }
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  isLoggedIn(): boolean {
    return this.userService.isLoggedIn();
  }

  goToLogin(): void {
    this.router.navigate(['/login']);
  }

  logout(): void {
    this.userService.logout();
  }

  dropdownClick(dropdownId): void {
    const dropdown = document.getElementById(dropdownId);
    if (dropdown.className.indexOf('w3-show') === -1) {
      $('.w3-dropdown-content').removeClass('w3-show');
      dropdown.className += ' w3-show';
    } else {
      $('.w3-dropdown-content').removeClass('w3-show');
    }
  }

  setLocale(locale: L10nLocale): void {
    this.translation.setLocale(locale);
  }
}
