import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {EarnComponent} from './cashbook/earn/earn.component';
import {CostComponent} from './cashbook/cost/cost.component';
import {EarnModule} from './cashbook/earn/earn.module';
import {CostModule} from './cashbook/cost/cost.module';
import {MessageAlertComponent} from './cashbook/shared/message-alert/message-alert.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {CashbookService} from './cashbook/shared/service/cashbook.service';
import {BaseHttpInterceptor} from './cashbook/shared/service/base-http-interceptor';
import {LoginComponent} from './cashbook/shared/login/login.component';
import {HomeComponent} from './cashbook/shared/home/home.component';
import {FormsModule} from '@angular/forms';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ReportModule} from './cashbook/report/report.module';
import {L10nIntlModule, L10nLoader, L10nTranslationModule, L10nValidationModule} from 'angular-l10n';
import * as l10n from './l10n-config';
import {AppStorage, HttpTranslationLoader} from './l10n-config';

@NgModule({
  declarations: [
    AppComponent,
    EarnComponent,
    CostComponent,
    MessageAlertComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    L10nTranslationModule.forRoot(l10n.l10nConfig, {storage: AppStorage, translationLoader: HttpTranslationLoader}), L10nIntlModule,
    L10nValidationModule.forRoot({validation: l10n.LocaleValidation}),
    EarnModule, CostModule, ReportModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: BaseHttpInterceptor, multi: true},
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: APP_INITIALIZER, useFactory: l10n.initL10n, deps: [L10nLoader], multi: true},
    CashbookService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
