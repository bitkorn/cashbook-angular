import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = '';
  passwd = '';

  constructor(
    private router: Router,
    public userService: UserService,
  ) {
  }

  ngOnInit(): void {
  }

  login(): void {
    this.userService.login(this.user, this.passwd);
  }

}
