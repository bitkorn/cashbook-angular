export class BaseApiResponse {
  constructor(
    public uuid: string,
    public desc: string,
    public success: number,
    public messages: [] = [],
    public val: string,
    public obj: {}, // single value
    public arr: [], // multiple values
    public count: number
  ) {
  }
}
