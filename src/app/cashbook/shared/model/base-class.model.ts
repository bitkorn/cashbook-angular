import {StringKeyMap} from '../service/tools.service';

export class BaseClass {

  exchangeObject(obj: StringKeyMap): void {
    const keys = Object.keys(obj);
    for (const key of keys) {
      if (!this.hasOwnProperty(key)) {
        continue;
      }
      this[key] = obj[key];
    }
  }

  getPlainobject(): StringKeyMap {
    const keys = Object.keys(this) as string[];
    const obj: StringKeyMap = {};
    for (const key of keys) {
      obj[key] = this[key];
    }
    return obj;
  }

  getAsFormData(): FormData {
    const keys = Object.keys(this);
    const form = new FormData();
    for (const key of keys) {
      if (!this.hasOwnProperty(key)) {
        continue;
      }
      form.append(key, this[key]);
    }
    return form;
  }

  htmlentitiesDecode(): void {
    const dp = new DOMParser();
    const keys = Object.keys(this);
    for (const key of keys) {
      if (!this.hasOwnProperty(key) || this[key] === null) {
        continue;
      }
      this[key] = dp.parseFromString(this[key], 'text/html').documentElement.textContent;
    }
  }

  getValueP(field: string): string {
    if (this.hasOwnProperty(field)) {
      const dp = new DOMParser();
      return dp.parseFromString(this[field], 'text/html').documentElement.textContent;
    }
    return '';
  }
}
