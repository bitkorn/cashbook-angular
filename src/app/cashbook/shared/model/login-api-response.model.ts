export class LoginApiResponse {
  constructor(
    public desc: string,
    public auth: number,
    public success: number,
    public messages: [],
    public obj: {}, // single value
    public arr: [], // multiple values
    public session_hash: string,
    public rightsnroles: {},
    public user_uuid: string,
    public user: {
      user_active: number,
      user_email: string,
      user_lang_iso: string,
      user_login: string,
      user_role_id: number,
      user_time_create: number,
      user_time_update: number,
      user_uuid: string,
    }
  ) {
  }
}
