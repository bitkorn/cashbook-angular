import {BaseClass} from './base-class.model';

export class User extends BaseClass {
  user_uuid = '';
  user_login = '';
  user_time_create = '';
  user_email = '';
  user_time_update = '';
  user_lang_iso = '';
}
