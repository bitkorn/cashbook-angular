import {Component, OnInit} from '@angular/core';
import {Message, MessageService} from '../service/message.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-message-alert',
  templateUrl: './message-alert.component.html',
  styleUrls: ['./message-alert.component.scss']
})
export class MessageAlertComponent {

  messageTimeout = 5000;
  message: Message;
  styleDisplay = 'none';

  constructor(public messageService: MessageService) {
    this.messageService.cashbookMessage.subscribe((message: Message) => {
      this.message = message;
      this.show();
    });
    this.messageService.cashbookFormMessages.subscribe((messages: string[]) => {
      this.glueMessagesOnElements(messages);
    });
    this.messageService.cashbookFormClear.subscribe((clear: boolean) => {
      if (clear) {
        this.clearMessagesOnElements();
      }
    });
  }

  show(): void {
    this.styleDisplay = 'block';
    window.setTimeout(() => this.hide(), this.messageTimeout);
  }

  hide(): void {
    this.styleDisplay = 'none';
  }

  glueMessagesOnElements(messages: string[]): void {
    this.clearMessagesOnElements();
    for (const message of messages) {
      const splittet = message.split('|#|');
      const elem = $('#' + splittet[0]);
      if (elem) {
        const tempElem = document.createElement('div');
        tempElem.style.color = 'red';
        tempElem.innerText = splittet[1];
        tempElem.classList.add('elemessage');
        $(elem).after(tempElem.outerHTML);
        $(elem).css('background-color', 'red');
      }
    }
  }

  clearMessagesOnElements(): void {
    $('.elemessage').remove();
    $('input').css('background-color', '');
    $('select').css('background-color', '');
    $('textarea').css('background-color', '');
  }
}
