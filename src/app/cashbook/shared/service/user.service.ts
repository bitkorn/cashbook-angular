import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Subject} from 'rxjs';
import {User} from '../model/user.model';
import {LoginApiResponse} from '../model/login-api-response.model';
import {BaseApiResponse} from '../model/base-api-response.model';
import {Rightsnroles} from '../model/rightsnroles.model';

export interface LoginState {
  loggedIn: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private state: LoginState = {loggedIn: false};
  private rightsnroles: Rightsnroles = null;
  private user: User = new User();

  private isLoginSource = new Subject<boolean>(); // ...to fire next()
  public isLogin = this.isLoginSource.asObservable(); // ...to listen for

  constructor(private cookieService: CookieService, private router: Router, private http: HttpClient) {
    const u = window.localStorage.getItem('user');
    if (!u) {
      this.state.loggedIn = false;
      this.user = new User();
    } else {
      this.user.exchangeObject(JSON.parse(u));
      this.state.loggedIn = true;
    }
  }

  get sessionhash(): string {
    if (this.cookieService.check('SESSIONHASH')) {
      return this.cookieService.get('SESSIONHASH');
    }
    return '';
  }

  set sessionhash(sessionhash: string) {
    /**
     *  sameSite: "Lax" | "None" | "Strict"
     */
    this.cookieService.set('SESSIONHASH', sessionhash, null, null, null, false, 'Lax');
  }

  handleLoginApiResponse(loginApiResponse: LoginApiResponse): void {
    if (loginApiResponse.success === 1) {
      this.state.loggedIn = true;
      this.sessionhash = loginApiResponse.session_hash;
      window.localStorage.setItem('rightsnroles', JSON.stringify(loginApiResponse.rightsnroles));
      this.rightsnroles = new Rightsnroles(loginApiResponse.rightsnroles);
      window.localStorage.setItem('user', JSON.stringify(loginApiResponse.user));
      this.user.exchangeObject(loginApiResponse.user);
    } else {
      this.handleLoggedOut();
    }
  }

  handleLoggedOut(): void {
    this.state.loggedIn = false;
    this.sessionhash = '';
    window.localStorage.removeItem('rightsnroles');
    this.rightsnroles = null;
    window.localStorage.removeItem('user');
    this.user = new User();
  }

  sessionCheck(): void {
    this.http.get<LoginApiResponse>('/session-check').subscribe((data: LoginApiResponse) => {
      this.handleLoginApiResponse(data);
      if (!this.state.loggedIn) {
        this.navigateLogin();
      }
    });
  }

  login(login: string, passwd: string): void {
    const formData = new FormData();
    formData.append('login', login);
    formData.append('passwd', passwd);
    this.http.post<LoginApiResponse>('/login', formData).subscribe((data) => {
      this.handleLoginApiResponse(data);
      if (this.state.loggedIn) {
        this.router.navigate(['/home']);
      } else {
        this.isLoginSource.next(false);
      }
    }, (error: HttpErrorResponse) => {
      if (error.status === 403) {
        this.isLoginSource.next(false);
      }
    });
  }

  logout(): void {
    this.http.get<BaseApiResponse>('/logout').subscribe((data) => {
      if (data.success === 1) {
        this.handleLoggedOut();
        this.navigateLogin();
      }
    });
  }

  isLoggedIn(): boolean {
    return this.state.loggedIn;
  }

  isUserInRole(roleId: number): boolean {
    if (this.rightsnroles === null) {
      return false;
    }
    return this.rightsnroles.roleId === roleId;
  }

  isUserInRoleMin(roleId: number): boolean {
    return this.rightsnroles.roleId <= roleId;
  }

  navigateLogin(): void {
    this.router.navigate(['/login']);
  }

  getUsername(): string {
    return this.user.user_login;
  }
}
