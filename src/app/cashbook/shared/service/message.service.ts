import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

export type MessageType = 'error' | 'warn' | 'success' | 'info';

export class Message {
  styleColor = '';
  styleBgColor = '';

  constructor(public messageType: MessageType, public label: string, public text: string) {
    switch (messageType) {
      case 'error':
        this.styleBgColor = 'red';
        this.styleColor = 'black';
        break;
      case 'warn':
        this.styleBgColor = '#ff9900';
        this.styleColor = 'black';
        break;
      case 'success':
        this.styleBgColor = '#009933';
        this.styleColor = 'white';
        break;
      case 'info':
        this.styleBgColor = '#0099ff';
        this.styleColor = 'white';
        break;
    }
  }
}

export const messs = {
  saveSuccess: 'Erfolgreich gespeichert',
  saveError: 'Fehler beim Speichern',
  deleteSuccess: 'Erfolgreich gelöscht',
  deleteError: 'Fehler beim Löschen',
  formInvalid: 'Nicht alle Eingaben sind valide.',
  fetchError: 'Fehler beim Holen der Daten',
  sendSuccess: 'Erfolgreich versendet',
  sendError: 'Fehler beim Senden',
};


@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private cashbookMessageSource = new Subject<Message>(); // ...to fire next()
  public cashbookMessage = this.cashbookMessageSource.asObservable(); // ...to listen for
  private cashbookFormMessagesSource = new Subject<string[]>(); // ...to fire next()
  public cashbookFormMessages = this.cashbookFormMessagesSource.asObservable(); // ...to listen for
  private cashbookFormClearSource = new Subject<boolean>(); // ...to fire next()
  public cashbookFormClear = this.cashbookFormClearSource.asObservable(); // ...to listen for

  constructor() {
  }

  showMessage(messageType: MessageType, label: string, text: string, withTimeout: boolean = true): void {
    this.cashbookMessageSource.next(new Message(messageType, label, text));
  }

  showMessages(messageType: MessageType, label: string, texts: string[], withTimeout: boolean = true): void {
    let text = texts.shift() as string;
    for (const t of texts) {
      text += '<br>' + t;
    }
    this.showMessage(messageType, label, text);
  }

  glueMessagesOnElements(messages: string[]): void {
    this.cashbookFormMessagesSource.next(messages);
  }

  clearMessagesOnElements(): void {
    this.cashbookFormClearSource.next(true);
  }
}
