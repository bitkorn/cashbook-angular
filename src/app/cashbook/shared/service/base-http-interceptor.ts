import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {UserService} from './user.service';
import {MessageService} from './message.service';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BaseHttpInterceptor implements HttpInterceptor {

  private totalRequests = 0;

  constructor(
    private cookieService: CookieService,
    private router: Router,
    private useruserService: UserService,
    public messageService: MessageService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.totalRequests++;
    if (req.urlWithParams.substring(0, 7) !== '/assets') {
      req = req.clone({
        url: environment.apiBaseUrl + req.url,
        withCredentials: true,
        setHeaders: {
          SESSIONHASH: this.cookieService.get('SESSIONHASH'),
        }
      });
    }

    return next.handle(req).pipe(
      tap((event) => {
        if (event instanceof HttpResponse) {
        }
        if (event instanceof HttpRequest) {
        }
        return event;
      }, error => {
        this.totalRequests--;
        if (error.status === 403) {
          if (this.useruserService.sessionhash.length > 0) {
            this.messageService.showMessage('warn', '', 'Es fehlen die erforderlichen Rechte.');
          } else {
            this.useruserService.handleLoggedOut();
            this.useruserService.navigateLogin();
          }
        } else if (error.status === 400) {
          const messs = ['Der Server gab den Fehler 400 zurück!'];
          if (error.error && error.error.messages && error.error.messages.length > 0) {
            error.error.messages.forEach((mess) => {
              messs.push(mess);
            });
          }
          this.messageService.showMessages('warn', '', messs);
        } else if (error.status > 299) {
          this.messageService.showMessage('error', '', 'Der Server gab den Fehler ' + error.status + ' zurück!');
        } else if (error.status === 201) {
          this.messageService.showMessage('success', '', 'Erfolgreich erstellt.');
        }
      }, () => {
        this.totalRequests--;
      }));
  }
}
