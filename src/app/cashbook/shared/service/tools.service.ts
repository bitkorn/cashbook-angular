import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {KeyValue} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  constructor() {
  }

  /**
   * Display errors in console.
   * Wird die Funktion als Callback uebergeben, ist this.messageService undefined!
   * @param err Die HttpErrorResponse vom Server
   */
  handleHttpErrorResponse(err: HttpErrorResponse): Observable<any> {
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', err.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(JSON.stringify(err, null, 4));
      console.error('Backend returned code: ' + err.status);
      console.error('body was: ' + err.error);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  computeQueryParamsFromSearchObject(searchObject: StringKeyMap): string {
    const searchKeys: string[] = Object.keys(searchObject);
    let queryParams = '?';
    let count = 0;
    for (const key of searchKeys) {
      if (searchObject.hasOwnProperty(key) && searchObject[key] && typeof searchObject[key] !== 'object') {
        if (count > 0) {
          queryParams += '&';
        }
        queryParams += key + '=' + searchObject[key];
      }
      count++;
    }
    return queryParams;
  }

  computeNumberStringMap(obj: StringKeyMap): Map<number, string> {
    const assoc = new Map<number, string>();
    const keys = Object.keys(obj);
    for (const key of keys) {
      if (obj.hasOwnProperty(key)) {
        assoc.set(+key, obj[key]);
      }
    }
    return assoc;
  }

  computeStringStringMap(obj: StringKeyMap): Map<string, string> {
    const assoc = new Map<string, string>();
    const keys = Object.keys(obj);
    for (const key of keys) {
      if (obj.hasOwnProperty(key)) {
        assoc.set(key, obj[key]);
      }
    }
    return assoc;
  }

  timeStringMinutesCreate(minutes: number): string | number {
    if (minutes === 0) {
      return minutes;
    }
    const hours = Math.floor(minutes / 60);
    const restMinutes = '0' + (minutes % 60);
    return hours + ':' + restMinutes.substr(-2);
  }

  timeStringMinutesParse(timeString: string): number {
    const splitted = timeString.split(':');
    if (splitted.length === 1) {
      return parseInt(timeString, 10);
    } else if (splitted.length !== 2) {
      return 0;
    }
    return parseInt(splitted[0], 10) * 60 + parseInt(splitted[1], 10);
  }

  decodeHTMLEntities(text): string {
    const textArea = document.createElement('textarea');
    textArea.innerHTML = text;
    return textArea.value;
  }

  htmlEntities(obj: object, fieldName: string, value: string): void {
    obj[fieldName] = this.decodeHTMLEntities(value);
  }

  encodeHTMLEntities(text): string {
    const textArea = document.createElement('textarea');
    textArea.innerText = text;
    return textArea.innerHTML;
  }

  countNewline(text: string): number {
    return text.split(/\n/).length;
  }

  /**
   * preserve functions
   * https://stackoverflow.com/questions/52793944/angular-keyvalue-pipe-sort-properties-iterate-in-order
   */

  // Preserve original property order
  originalOrder(a: KeyValue<string, string>, b: KeyValue<string, string>): number {
    return 0;
  }

  // Order by ascending property value
  valueAscOrder(a: KeyValue<string, string>, b: KeyValue<string, string>): number {
    return a.value.localeCompare(b.value);
  }

  // Order by descending property key
  keyDescOrder(a: KeyValue<string, string>, b: KeyValue<string, string>): number {
    return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
  }
}

export interface StringKeyMap {
  [key: string]: any;
}
