import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MessageService} from './message.service';
import {ToolsService} from './tools.service';
import {Observable, Subject} from 'rxjs';
import {BaseApiResponse} from '../model/base-api-response.model';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CashbookService {

  private isLoadingSource = new Subject<boolean>(); // ...to fire next()
  public isLoading = this.isLoadingSource.asObservable(); // ...to listen for

  constructor(
    public http: HttpClient,
    private messageService: MessageService,
    private serviceTools: ToolsService
  ) {
  }

  setLoading(isLoading: boolean): void {
    this.isLoadingSource.next(isLoading);
  }

  getCostTypes(): Observable<[]> {
    return this.http.get<BaseApiResponse>('/cashbook-lists-costtype').pipe(map(
      response => {
        return response.arr;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }
}
