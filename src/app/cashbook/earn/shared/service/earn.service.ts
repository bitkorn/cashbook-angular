import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BaseApiResponse} from '../../../shared/model/base-api-response.model';
import {catchError, map} from 'rxjs/operators';
import {MessageService} from '../../../shared/service/message.service';
import {ToolsService} from '../../../shared/service/tools.service';
import {Earn} from '../model/earn.model';

@Injectable({
  providedIn: 'root'
})
export class EarnService {

  constructor(
    public http: HttpClient,
    private messageService: MessageService,
    private serviceTools: ToolsService
  ) {
  }

  getEarns(clientUuid: string, dateFrom: string, dateTo: string): Observable<[]> {
    return this.http.get<BaseApiResponse>(
      '/cashbook-rest-accounting-earn?client_uuid=' + clientUuid + '&date_from=' + dateFrom + '&date_to=' + dateTo).pipe(map(
      response => {
        return response.arr;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  insertEarn(earn: Earn): Observable<boolean> {
    this.messageService.clearMessagesOnElements();
    return this.http.post<BaseApiResponse>('/cashbook-rest-accounting-earn', earn).pipe(map(
      response => {
        if (response.success === 1) {
          return true;
        } else {
          this.messageService.glueMessagesOnElements(response.messages);
        }
        return false;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }
}
