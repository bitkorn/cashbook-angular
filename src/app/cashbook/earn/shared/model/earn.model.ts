import {BaseClass} from '../../../shared/model/base-class.model';

export class Earn extends BaseClass {
  earn_uuid = '';
  client_uuid = '';
  earn_label = '';
  earn_net = 0;
  earn_taxp = 19;
  earn_gross = 0;
  earn_date = '';
  earn_docno = '';

  calculateGross(): void {
    this.earn_gross = +this.earn_net + this.earn_net * (this.earn_taxp / 100);
  }
}
