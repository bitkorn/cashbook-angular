import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EarnRoutingModule } from './earn-routing.module';
import { EarnAddComponent } from './earn-add/earn-add.component';
import {FormsModule} from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';


@NgModule({
  declarations: [EarnAddComponent],
  imports: [
    CommonModule,
    EarnRoutingModule,
    FormsModule,
    DpDatePickerModule
  ]
})
export class EarnModule { }
