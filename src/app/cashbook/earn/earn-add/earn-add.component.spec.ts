import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EarnAddComponent } from './earn-add.component';

describe('EarnAddComponent', () => {
  let component: EarnAddComponent;
  let fixture: ComponentFixture<EarnAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EarnAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EarnAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
