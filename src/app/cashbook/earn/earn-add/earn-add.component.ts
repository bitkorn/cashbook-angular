import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import * as appPrefs from '../../../app-prefs';
import {Earn} from '../shared/model/earn.model';
import {EarnService} from '../shared/service/earn.service';
import {CashbookService} from '../../shared/service/cashbook.service';
import {MessageService, messs} from '../../shared/service/message.service';

@Component({
  selector: 'app-earn-add',
  templateUrl: './earn-add.component.html',
  styleUrls: ['./earn-add.component.scss']
})
export class EarnAddComponent implements OnInit, OnDestroy {
  subscriptions = new Set<Subscription>();
  earn: Earn = new Earn();
  public datePickerConf = appPrefs.datePickerConf;
  dateFrom = '';
  dateTo = '';
  earnSummarys = [];

  constructor(
    private earnService: EarnService,
    private cashbookService: CashbookService,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  insertCost(): void {
    this.earn.client_uuid = appPrefs.client.uuid;
    this.subscriptions.add(this.earnService.insertEarn(this.earn).subscribe((data: boolean) => {
      if (data) {
        this.messageService.showMessage('success', '', messs.saveSuccess);
        this.loadSummary();
      }
      this.earn.earn_docno = '';
    }));
  }

  loadSummary(): void {
    this.subscriptions.add(this.earnService.getEarns(appPrefs.client.uuid, this.dateFrom, this.dateTo).subscribe((data: []) => {
      this.earnSummarys = data;
    }));
  }
}
