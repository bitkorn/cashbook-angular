import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EarnComponent} from './earn.component';
import {EarnAddComponent} from './earn-add/earn-add.component';

const routes: Routes = [
  {path: 'earn', component: EarnComponent, children: [
      {path: 'add', component: EarnAddComponent},
    ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EarnRoutingModule { }
