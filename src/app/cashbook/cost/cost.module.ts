import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CostRoutingModule} from './cost-routing.module';
import {CostAddComponent} from './cost-add/cost-add.component';
import {CostService} from './shared/service/cost.service';
import {FormsModule} from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';


@NgModule({
  declarations: [CostAddComponent],
  imports: [
    CommonModule,
    DpDatePickerModule,
    CostRoutingModule,
    FormsModule
  ],
  providers: [CostService]
})
export class CostModule {
}
