import {Component, OnDestroy, OnInit} from '@angular/core';
import {CostService} from '../shared/service/cost.service';
import {Subscription} from 'rxjs';
import {Cost} from '../shared/model/cost.model';
import {MessageService, messs} from '../../shared/service/message.service';
import {CashbookService} from '../../shared/service/cashbook.service';
import * as appPrefs from '../../../app-prefs';

@Component({
  selector: 'app-cost-add',
  templateUrl: './cost-add.component.html',
  styleUrls: ['./cost-add.component.scss']
})
export class CostAddComponent implements OnInit, OnDestroy {
  subscriptions = new Set<Subscription>();
  cost: Cost = new Cost();
  costTypes: [] = [];
  public datePickerConf = appPrefs.datePickerConf;
  dateFrom = '';
  dateTo = '';
  costSummarys = [];

  constructor(
    private costService: CostService,
    private cashbookService: CashbookService,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.cashbookService.getCostTypes().subscribe((data: []) => {
      this.costTypes = data;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  insertCost(): void {
    this.cost.client_uuid = appPrefs.client.uuid;
    this.subscriptions.add(this.costService.insertCost(this.cost).subscribe((data: boolean) => {
      if (data) {
        this.messageService.showMessage('success', '', messs.saveSuccess);
        this.loadSummary();
      }
      this.cost.cost_docno = '';
    }));
  }

  loadSummary(): void {
    this.subscriptions.add(this.costService.getCosts(appPrefs.client.uuid, this.dateFrom, this.dateTo).subscribe((data: []) => {
      this.costSummarys = data;
    }));
  }
}
