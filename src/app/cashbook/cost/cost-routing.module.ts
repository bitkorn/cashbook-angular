import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CostComponent} from './cost.component';
import {CostAddComponent} from './cost-add/cost-add.component';

const routes: Routes = [
  {
    path: 'cost', component: CostComponent, children: [
      {path: 'add', component: CostAddComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostRoutingModule {
}
