import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {BaseApiResponse} from '../../../shared/model/base-api-response.model';
import {catchError, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {MessageService} from '../../../shared/service/message.service';
import {ToolsService} from '../../../shared/service/tools.service';
import {Cost} from '../model/cost.model';

@Injectable({
  providedIn: 'root'
})
export class CostService {

  constructor(
    public http: HttpClient,
    private messageService: MessageService,
    private serviceTools: ToolsService
  ) {
  }

  getCosts(clientUuid: string, dateFrom: string, dateTo: string): Observable<[]> {
    return this.http.get<BaseApiResponse>(
      '/cashbook-rest-accounting-cost?client_uuid=' + clientUuid + '&date_from=' + dateFrom + '&date_to=' + dateTo).pipe(map(
      response => {
        return response.arr;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }

  insertCost(cost: Cost): Observable<boolean> {
    this.messageService.clearMessagesOnElements();
    return this.http.post<BaseApiResponse>('/cashbook-rest-accounting-cost', cost).pipe(map(
      response => {
        if (response.success === 1) {
          return true;
        } else {
          this.messageService.glueMessagesOnElements(response.messages);
        }
        return false;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }
}
