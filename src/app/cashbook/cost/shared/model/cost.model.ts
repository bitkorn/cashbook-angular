import {BaseClass} from '../../../shared/model/base-class.model';

export class Cost extends BaseClass {
  cost_uuid = '';
  client_uuid = '';
  cost_type = 'other';
  cost_label = '';
  cost_net = 0;
  cost_taxp = 19;
  cost_gross = 0;
  cost_date = '';
  cost_docno = '';

  calculateGross(): void {
    this.cost_gross = +this.cost_net + this.cost_net * (this.cost_taxp / 100);
  }
}
