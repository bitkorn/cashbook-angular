import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReportRoutingModule} from './report-routing.module';
import {ReportComponent} from './report.component';
import {ReportCreateComponent} from './report-create/report-create.component';
import {FormsModule} from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';
import {L10nIntlModule, L10nTranslationModule} from 'angular-l10n';


@NgModule({
  declarations: [ReportComponent, ReportCreateComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    FormsModule,
    DpDatePickerModule,
    L10nIntlModule,
    L10nTranslationModule,
  ]
})
export class ReportModule {
}
