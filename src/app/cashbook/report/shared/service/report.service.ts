import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MessageService} from '../../../shared/service/message.service';
import {ToolsService} from '../../../shared/service/tools.service';
import {Observable} from 'rxjs';
import {BaseApiResponse} from '../../../shared/model/base-api-response.model';
import {catchError, map} from 'rxjs/operators';
import {Report} from '../model/report.model';
import {UserService} from '../../../shared/service/user.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    public http: HttpClient,
    private messageService: MessageService,
    private serviceTools: ToolsService,
    private userService: UserService
  ) {
  }

  getReport(clientUuid: string, dateFrom: string, dateTo: string): Observable<Report> {
    return this.http.get<BaseApiResponse>(
      '/cashbook-report-create?client_uuid=' + clientUuid + '&date_from=' + dateFrom + '&date_to=' + dateTo).pipe(map(
      response => {
        return response.obj as Report;
      }
    ), catchError(this.serviceTools.handleHttpErrorResponse));
  }
}
