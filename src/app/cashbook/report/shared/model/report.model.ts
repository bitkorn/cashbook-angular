import {BaseClass} from '../../../shared/model/base-class.model';

export class Report extends BaseClass {
  list_earn: [
    {
      earn_uuid: '';
      client_uuid: '';
      earn_label: '';
      earn_net: 0;
      earn_taxp: 0;
      earn_gross: 0;
      earn_date: '';
      earn_docno: '';
      earn_time_create: '';
    }
  ];
  list_cost: [
    {
      cost_uuid: '';
      client_uuid: '';
      cost_type: '';
      cost_label: '';
      cost_net: 0;
      cost_taxp: 0;
      cost_gross: 0;
      cost_date: '';
      cost_docno: '';
      cost_time_create: '';
    }
  ];
  sum_earn: [
    {
      sum_net: 0;
      sum_gross: 0;
    }
  ];
  sum_cost: [
    {
      cost_type: '';
      sum_net: 0;
      sum_gross: 0;
    }
  ];
}
