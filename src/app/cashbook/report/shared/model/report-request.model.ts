import {BaseClass} from '../../../shared/model/base-class.model';

export class ReportRequest extends BaseClass {
  date_from = '';
  date_to = '';
}
