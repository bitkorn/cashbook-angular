import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReportComponent} from './report.component';
import {ReportCreateComponent} from './report-create/report-create.component';

const routes: Routes = [
  {
    path: 'report', component: ReportComponent, children: [
      {path: 'create', component: ReportCreateComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule {
}
