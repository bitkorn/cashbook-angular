import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ReportService} from '../shared/service/report.service';
import {CashbookService} from '../../shared/service/cashbook.service';
import {MessageService} from '../../shared/service/message.service';
import * as appPrefs from '../../../app-prefs';
import {Subscription} from 'rxjs';
import {Report} from '../shared/model/report.model';
import {L10N_LOCALE, L10nLocale, L10nValidation} from 'angular-l10n';
import {UserService} from '../../shared/service/user.service';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-report-create',
  templateUrl: './report-create.component.html',
  styleUrls: ['./report-create.component.scss']
})
export class ReportCreateComponent implements OnInit, OnDestroy {
  subscriptions = new Set<Subscription>();
  public datePickerConf = appPrefs.datePickerConf;
  dateFrom = '2022-01-01';
  dateTo = '2022-03-31';
  report: Report = new Report();
  clientUuid = appPrefs.client.uuid;
  sessionHash = '';
  apiUrl = environment.apiBaseUrl;

  taxsSum = 0;

  constructor(
    private reportService: ReportService,
    private cashbookService: CashbookService,
    private messageService: MessageService,
    @Inject(L10N_LOCALE) public locale: L10nLocale,
    private l10nValidation: L10nValidation,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    this.sessionHash = this.userService.sessionhash;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  createReport(): void {
    this.subscriptions.add(this.reportService.getReport(this.clientUuid, this.dateFrom, this.dateTo).subscribe((data: Report) => {
      this.report = data;
      this.taxsSum = 0;
      this.report.sum_cost.forEach(rep => {
        this.taxsSum += (rep.sum_gross - rep.sum_net);
      });
    }));
  }

}
