
## generate code scaffolding
[angular.io/cli/generate](https://angular.io/cli/generate)

generate a new component|directive|pipe|service|class|guard|interface|enum|module:
```shell
####################
# START at /src/app
####################
# generate module with routing
ng g module cashbook/trinket --routing
# component
ng generate component cashbook/trinket/...
# model
ng g class cashbook/trinket/... --type=model --skipTests=true
```

## run|build|etc.
```shell
# add a module
ng add @angular/material
# serve
ng serve
# build
ng build
ng build --configuration production
# with localization (/src/locale/messages.*.xlf) files
ng build --configuration production --localize
# execute the unit tests via [Karma](https://karma-runner.github.io)
ng e2e
```

## install
[nodesource/README.md](https://github.com/nodesource/distributions/blob/master/README.md)
```shell
# update npm
npm install -g npm
# 
npm install -g @angular/cli
# update
npm update
ng update
# update angular
ng update @angular/core @angular/cli
ng update @angular/cli
ng update @angular/core
ng update rxjs
```

**Version checks**
```shell
ng v
# TypeScript Version check
tsc -v
```

Name                               Version                  Command to update
--------------------------------------------------------------------------------
@angular/cli                       11.1.1 -> 11.2.4         ng update @angular/cli
@angular/core                      11.1.1 -> 11.2.5         ng update @angular/core
@angular/material                  11.1.1 -> 11.2.4         ng update @angular/material
@angular/cdk                       11.1.1 -> 11.2.4         ng update @angular/cdk
@ngrx/store                        10.1.2 -> 11.0.1         ng update @ngrx/store
rxjs                               6.6.3 -> 6.6.6           ng update rxjs

```shell
# upgrade all
ng update @angular/core@15 @angular/cli@15 @angular/cdk@15 @angular/material@15 @angular-devkit/build-angular@15 @angular/animations@15 @angular/common@15 ngx-cookie-service@15 ng2-date-picker@15 @angular/compiler-cli@15
npm install angular-l10n@15
npm i typescript@latest -D
```

## i18n
[angular.io/i18n#create-a-translation-source-file](https://angular.io/guide/i18n#create-a-translation-source-file)

generate source file:
```shell
ng xi18n --output-path src/locale --i18n-locale de
```
The following message (after run the above command) is hopefully not sogned to me: Option "i18nLocale" is deprecated: Use 'i18n' project level sub-option 'sourceLocale' instead.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

[Angular docs](https://angular.io/docs)

[angular/cli/README.md](https://github.com/angular/angular-cli/blob/master/packages/angular/cli/README.md)

[CookieService](https://www.npmjs.com/package/ngx-cookie-service)

[angular-svg-icon](https://www.npmjs.com/package/angular-svg-icon)

[fontawesome/angular](https://fontawesome.com/how-to-use/on-the-web/using-with/angular)

[globalize](https://github.com/globalizejs/globalize)

[angular-l10n](https://robisim74.github.io/angular-l10n/)
- [stackblitz](https://stackblitz.com/edit/angular-l10n)

[angular.io/NgForOf](https://angular.io/api/common/NgForOf)

[angular.io/NgIf](https://angular.io/api/common/NgIf)

[ngrx.io/docs](https://ngrx.io/docs)

[ng2-date-picker](https://www.npmjs.com/package/ng2-date-picker)

[angular-mydatepicker](https://github.com/kekeh/angular-mydatepicker)
- unused

[CKEditor Angular](https://ckeditor.com/docs/ckeditor5/latest/builds/guides/integration/frameworks/angular.html)


# Angular Buch dpunkt
[book-monkey3.angular-buch.com](https://book-monkey3.angular-buch.com/)

[github.com/book-monkey3](https://github.com/book-monkey3)

